sickle = {}

sickle.sickle_on_use = function (itemstack, digger, pointed_thing, sickle_def)
	if pointed_thing.type ~= "node" then return end
	local pos = pointed_thing.under
	
	if not digger then return end
	
	local yaw = digger:get_look_horizontal() * 180 / math.pi
	
	-- Find which direction the player is pointing.
	local facing = {x = 0, y = 0, z = 0}
	if yaw < 45 or yaw >= 45*7 then
		facing.z = 1
	elseif yaw < 45*3 then
		facing.x = -1
	elseif yaw < 45*5 then
		facing.z = -1
	elseif yaw < 45*7 then
		facing.x = 1
	end
	
	if facing.x == 0 and facing.y == 0 and facing.z == 0 then return end
	
	local node = minetest.get_node(pos)
	local def = minetest.registered_nodes[node.name]
	
	-- If it was the soil that was clicked, look above it.
	if def and def.groups and def.groups.soil and def.groups.soil > 0 then
		pos = vector.add(pos, {x = 0, y = 1, z = 0})
	end
	
	-- For a square with the defined radius...
	for x = -sickle_def.radius, sickle_def.radius do
		for z = -sickle_def.radius, sickle_def.radius do
			local pos = vector.add(pos, {x = x, y = 0, z = z})
			local node = minetest.get_node(pos)
			local def = minetest.registered_nodes[node.name]
			
			-- If we're no longer on top of soil, stop harvesting.
			local on_soil
			do
				local below = minetest.get_node(vector.subtract(pos, {x = 0, y = 1, z = 0}))
				local def = minetest.registered_nodes[below.name]
				
				on_soil = def and def.groups and def.groups.soil and def.groups.soil > 0
			end
			
			-- If the node is a fully grown plant, harvest it.
			if on_soil and not minetest.is_protected(pos, digger:get_player_name()) and def and def.groups and def.groups.plant and def.groups.plant > 0 then
				local data = farming.plant_stages[node.name]
				
				if #data.stages_left < 1 then
					(def.on_dig or minetest.node_dig)(pos, node, digger)
				end
			end
		end
	end
	
	itemstack:add_wear(65535 / sickle_def.max_uses)
	return itemstack
end

sickle.register_sickle = function (name, def)
	-- Check for : prefix (register new hoes in your mod's namespace)
	if name:sub(1,1) ~= ":" then
		name = ":" .. name
	end
	
	-- Check def table
	if def.description == nil then
		def.description = "Sickle"
	end
	if def.inventory_image == nil then
		def.inventory_image = "unknown_item.png"
	end
	if def.max_uses == nil then
		def.max_uses = 30
	end
	--[[if def.max_level == nil then
		def.max_level = 2
	end]]
	if def.radius == nil then
		def.radius = 16
	end
	--[[if def.tool_capabilites == nil then
		def.tool_capabilities = {
			full_punch_interval = 0.8,
			max_drop_level=1,
			groupcaps={
				snappy={times={[1]=def.time * 2, [2]=def.time, [3]=def.time / 3}, uses=def.max_uses, maxlevel=def.max_level},
			},
			damage_groups = {fleshy=4},
		}
	end]]
	
	-- Register the tool
	minetest.register_tool(name, {
		description = def.description,
		inventory_image = def.inventory_image,
		tool_capabilities = def.tool_capabilities,
		on_use = function (itemstack, user, pointed_thing)
			return sickle.sickle_on_use(itemstack, user, pointed_thing, def)
		end,
		groups = def.groups,
		sound = {breaks = "default_tool_breaks"},
	})
	
	-- Register its recipe
	if def.recipe then
		minetest.register_craft({
			output = name:sub(2),
			recipe = def.recipe
		})
	elseif def.material then
		minetest.register_craft({
			output = name:sub(2),
			recipe = {
				{def.material, def.material, ""},
				{"", "group:stick", def.material},
				{"", "group:stick", ""}
			}
		})
	end
end

sickle.register_sickle("sickle:wood", {
	description = "Wood Sickle",
	inventory_image = "sickle_wood.png",
	
	max_uses = 60,
	radius = 3,
	
	material = "group:wood"
})

sickle.register_sickle("sickle:stone", {
	description = "Stone Sickle",
	inventory_image = "sickle_stone.png",
	
	max_uses = 90,
	radius = 5,
	
	material = "group:stone"
})

sickle.register_sickle("sickle:steel", {
	description = "Steel Sickle",
	inventory_image = "sickle_steel.png",
	
	max_uses = 150,
	radius = 7,
	
	material = "default:steel_ingot"
})

sickle.register_sickle("sickle:bronze", {
	description = "Bronze Sickle",
	inventory_image = "sickle_bronze.png",
	
	max_uses = 300,
	radius = 9,
	
	material = "default:bronze_ingot"
})

sickle.register_sickle("sickle:mese", {
	description = "Mese Sickle",
	inventory_image = "sickle_mese.png",
	
	max_uses = 350,
	radius = 11,
	
	material = "default:mese_crystal"
})

sickle.register_sickle("sickle:diamond", {
	description = "Diamond Sickle",
	inventory_image = "sickle_diamond.png",
	
	max_uses = 500,
	radius = 13,
	
	material = "default:diamond"
})